package yaccc

import (
	"context"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_DatabaseMock_Exist(t *testing.T) {
	mock := new(DatabaseMock)

	mock.On("Exist").Return(false, errors.New("some-error"))

	exist, err := mock.Exist(context.Background())
	assert.False(t, exist)
	assert.EqualError(t, err, "some-error")

	mock.AssertExpectations(t)
}

func Test_DatabaseMock_Info(t *testing.T) {
	mock := new(DatabaseMock)

	mock.On("Info").Return(nil, errors.New("some-error"))

	infos, err := mock.Info(context.Background())
	assert.Nil(t, infos)
	assert.EqualError(t, err, "some-error")

	mock.AssertExpectations(t)
}

func Test_DatabaseMock_Create(t *testing.T) {
	mock := new(DatabaseMock)

	mock.On("Create").Return(errors.New("some-error"))

	err := mock.Create(context.Background())
	assert.EqualError(t, err, "some-error")

	mock.AssertExpectations(t)
}

func Test_DatabaseMock_Ensure(t *testing.T) {
	mock := new(DatabaseMock)

	mock.On("Ensure").Return(errors.New("some-error"))

	err := mock.Ensure(context.Background())
	assert.EqualError(t, err, "some-error")

	mock.AssertExpectations(t)
}

func Test_DatabaseMock_Destroy(t *testing.T) {
	mock := new(DatabaseMock)

	mock.On("Destroy").Return(errors.New("some-error"))

	err := mock.Destroy(context.Background())
	assert.EqualError(t, err, "some-error")

	mock.AssertExpectations(t)
}

func Test_DatabaseMock_Set(t *testing.T) {
	type doc struct {
		Key string
	}

	dbMock := new(DatabaseMock)

	dbMock.On("Set", "some-id", "", doc{Key: "message"}).Return("some-rev", nil).Once()

	rev, err := dbMock.Set(context.Background(), "some-id", "", doc{Key: "message"})
	assert.NoError(t, err)
	assert.Equal(t, "some-rev", rev)

	dbMock.AssertExpectations(t)
}

func Test_DatabaseMock_Get(t *testing.T) {
	type doc struct {
		Key string
	}

	dbMock := new(DatabaseMock)

	dbMock.On("Get", "some-id").Return(`{"key": "message"}`, "some-rev", nil).Once()

	var res doc
	rev, err := dbMock.Get(context.Background(), "some-id", &res)
	assert.NoError(t, err)
	assert.Equal(t, "some-rev", rev)
	assert.Equal(t, "message", res.Key)

	dbMock.AssertExpectations(t)
}

func Test_DatabaseMock_Get_with_invalid_json_should_return_error(t *testing.T) {
	type doc struct {
		Key string
	}

	dbMock := new(DatabaseMock)

	dbMock.On("Get", "some-id").Return(`invalid-json`, "some-rev", nil).Once()

	var res doc
	rev, err := dbMock.Get(context.Background(), "some-id", &res)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to unmarshal body: invalid character 'i' looking for beginning of value")

	dbMock.AssertExpectations(t)
}

func Test_DatabaseMock_Find(t *testing.T) {
	type doc struct {
		Key string
	}

	dbMock := new(DatabaseMock)
	var res doc

	dbRes, err := NewFindResult(strings.NewReader(`{
		"docs": [
			{"_id": "some-id-1", "_rev": "some-rev-1", "key": "value1"},
			{"_id": "some-id-2", "_rev": "some-rev-2", "key": "value2"}
		]
	}`))
	assert.NoError(t, err)

	dbMock.On("Find", 42).Return(dbRes, nil).Once()

	results, err := dbMock.Find(context.Background(), "some-query: %d", 42)
	assert.NoError(t, err)
	assert.NotNil(t, results)

	// Unmarshal the first document
	assert.True(t, results.Next())
	id, rev, err := results.UnmarshalNext(&res)
	assert.NoError(t, err)
	assert.Equal(t, "some-id-1", id)
	assert.Equal(t, "some-rev-1", rev)
	assert.Equal(t, "value1", res.Key)

	// Unmarshal the second document
	assert.True(t, results.Next())
	id, rev, err = results.UnmarshalNext(&res)
	assert.NoError(t, err)
	assert.Equal(t, "some-id-2", id)
	assert.Equal(t, "some-rev-2", rev)
	assert.Equal(t, "value2", res.Key)

	dbMock.AssertExpectations(t)
}
