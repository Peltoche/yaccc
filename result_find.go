package yaccc

import (
	"encoding/json"
	"io"

	"github.com/pkg/errors"
)

// FindResult is the Result of a 'Find' query.
//
// It contains several documents which can be unmarshaled with a call to
// UnmarshalNext.
type FindResult struct {
	Bookmark string            `json:"bookmark"`
	Docs     []json.RawMessage `json:"docs"`
	Warning  string            `json:"warning"`
}

// NewFindResult instantiate aa new FindResult.
func NewFindResult(raw io.Reader) (*FindResult, error) {
	var res FindResult

	err := json.NewDecoder(raw).Decode(&res)
	if err != nil {
		return nil, errors.Wrap(err, "fail to unmarshal the response body")
	}

	return &res, nil
}

// Next return true is the next call document can be retrieved or not.
func (t *FindResult) Next() bool {
	return len(t.Docs) > 0
}

// UnmarshalNext retrieve the next document and fill valuePtr with.
//
// The value will be deserialized using 'json.Unmarshal' so the 'json' tags set
// on your struct will be take in account.
func (t *FindResult) UnmarshalNext(valuePtr interface{}) (string, string, error) {
	type metaDatas struct {
		ID  string `json:"_id"`
		Rev string `json:"_rev"`
	}

	nextRawResult := t.Docs[0]
	t.Docs = t.Docs[1:]

	var meta metaDatas
	err := json.Unmarshal(nextRawResult, &meta)
	if err != nil {
		return "", "", errors.Wrap(err, "fail to unmarshal the document metadata")
	}

	err = json.Unmarshal(nextRawResult, &valuePtr)
	if err != nil {
		return "", "", errors.Wrap(err, "fail to unmarshal the document content")
	}

	return meta.ID, meta.Rev, nil
}
