package yaccc

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

//var ClientURL = "http://localhost:5984"
//var DatabaseName = "test-database"

type mockMessage struct {
	Message string `json:"message"`
}

type unmarshalbleStruct struct {
	Chan chan<- struct{} `json:"chan"`
}

func initDatabase(t *testing.T) (*Database, *Client) {
	t.Helper()

	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	dbName := generateDBName(t)
	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: dbName})
	assert.NoError(t, err)

	return db, server
}

func Test_Database_String(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	assert.Equal(t, db.Name(), db.String())
}

func Test_Database_SetTimeout(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	db.SetTimeout(time.Hour)
	assert.Equal(t, time.Hour, db.timeout)
}

func Test_Database_Set_with_invalid_struct(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	rev, err := db.Set(context.Background(), "foobar", "", &unmarshalbleStruct{})
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to encode document: json: unsupported type: chan<- struct {}")
}

func Test_Database_Set_with_invalid_type(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	rev, err := db.Set(context.Background(), "foobar", "", "")
	assert.Empty(t, rev)
	assert.EqualError(t, err, "value must be a &struct")
}

func Test_Database_Set_and_Get(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	msg := mockMessage{"some-content"}

	rev, err := db.Set(context.Background(), "foobar", "", &msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)

	var res mockMessage
	rev, err = db.Get(context.Background(), "foobar", &res)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)
	assert.Equal(t, "some-content", res.Message)
}

func Test_Database_Set_twice_and_Get(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	msg := mockMessage{"some-content"}

	// Set 1
	rev, err := db.Set(context.Background(), "foobar", "", &msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)

	// Set 2
	msg = mockMessage{"some-content-2"}
	rev, err = db.Set(context.Background(), "foobar", rev, &msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)

	// Get
	var res mockMessage
	rev, err = db.Get(context.Background(), "foobar", &res)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)
	assert.Equal(t, "some-content-2", res.Message)
}

func Test_Database_Get_with_invalid_object(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	msg := mockMessage{"some-content"}

	// Set
	rev, err := db.Set(context.Background(), "foobar", "", &msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)

	// Get with a invalid object (string instead of mockMessage struct)
	var res string
	rev, err = db.Get(context.Background(), "foobar", &res)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to unmarshal body: json: cannot unmarshal object into Go value of type string")
}

func Test_Database_Set_with_empty_id(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	rev, err := db.Set(context.Background(), "", "", &mockMessage{"some-content"})
	assert.Empty(t, rev)
	assert.EqualError(t, err, "id empty")
}

func Test_Database_Set_with_transport_error(t *testing.T) {
	url, err := url.ParseRequestURI("http://unreachable-url/some-database")
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	rev, err := db.Set(context.Background(), "some-id", "", &mockMessage{"some-content"})
	assert.Empty(t, rev)
	assert.EqualError(t, err, fmt.Sprintf("fail to send request: Put %s/some-id: dial tcp: lookup unreachable-url: no such host", url))
}

func Test_Database_Set_with_server_error(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	rev, err := db.Set(context.Background(), "some-id", "", &mockMessage{"some-content"})
	assert.Empty(t, rev)
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}

func Test_Database_Get_with_server_error(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	var res mockMessage
	rev, err := db.Get(context.Background(), "some-id", &res)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}

func Test_Database_Get_with_unreachable_url(t *testing.T) {
	url, err := url.ParseRequestURI("http://unreachable-url")
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	var res mockMessage
	rev, err := db.Get(context.Background(), "some-id", &res)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to send request: Get http://unreachable-url/some-database/some-id: dial tcp: lookup unreachable-url: no such host")
}

func Test_Database_Get_with_empty_id(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	var res mockMessage
	rev, err := db.Get(context.Background(), "", &res)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "id empty")
}

func Test_Database_Find(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	rev, err := db.Set(context.Background(), "some-id", "", &mockMessage{
		Message: "some-content",
	})
	assert.NotEmpty(t, rev)
	assert.NoError(t, err)

	resFind, err := db.Find(context.Background(), `{
"selector": {
"message": {
"$eq": "some-content"
}
}
}`)
	assert.NoError(t, err)

	var res mockMessage

	assert.True(t, resFind.Next())
	id, rev, err := resFind.UnmarshalNext(&res)
	assert.Equal(t, "some-id", id)
	assert.NotEmpty(t, rev)
	assert.NoError(t, err)

	assert.False(t, resFind.Next())
}

func Test_Database_Find_with_invalid_valueptr(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	rev, err := db.Set(context.Background(), "some-id", "", &mockMessage{
		Message: "some-content",
	})
	assert.NotEmpty(t, rev)
	assert.NoError(t, err)

	resFind, err := db.Find(context.Background(), `{
"selector": {
"message": {
"$eq": "some-content"
}
}
}`)
	assert.NoError(t, err)

	// Find with invalid valuePtr (string insteado of mockMessage struct)
	var res string
	assert.True(t, resFind.Next())
	id, rev, err := resFind.UnmarshalNext(&res)
	assert.Empty(t, id)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to unmarshal the document content: json: cannot unmarshal object into Go value of type string")

	assert.False(t, resFind.Next())
}

func Test_Database_Find_with_invalid_meta(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		// Missing the '_id' and invalid '_rev' format
		w.Write([]byte(`{
"docs": [{"_rev": 32, "some-key": "some-value"}]
}`))
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	results, err := db.Find(context.Background(), "some-query")
	assert.NoError(t, err)

	var res mockMessage
	id, rev, err := results.UnmarshalNext(&res)
	assert.Empty(t, id)
	assert.Empty(t, rev)
	assert.EqualError(t, err, "fail to unmarshal the document metadata: json: cannot unmarshal number into Go struct field metaDatas._rev of type string")
}

func Test_Database_Find_with_server_error(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	res, err := db.Find(context.Background(), "some-query")
	assert.Nil(t, res)
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}

func Test_Database_Find_with_unreachable_url(t *testing.T) {
	url, err := url.ParseRequestURI("http://unreachable-url")
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	res, err := db.Find(context.Background(), "some-query")
	assert.Nil(t, res)
	assert.EqualError(t, err, "fail to send request: Post http://unreachable-url/some-database/_find: dial tcp: lookup unreachable-url: no such host")
}

func Test_Database_Find_with_invalid_server_response_body(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("unexpected-response"))
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	// Find with a invalid object (string instead of mockMessage struct)
	resFind, err := db.Find(context.Background(), `{
		"selector": {
			"message": {
				"$eq": "some-content"
			}
		}
	}`)
	assert.Empty(t, resFind)
	assert.EqualError(t, err, "fail to unmarshal the response body: invalid character 'u' looking for beginning of value")
}

func Test_Database_Delete(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	msg := mockMessage{"some-content"}

	rev, err := db.Set(context.Background(), "foobar", "", &msg)
	assert.NoError(t, err)
	assert.NotEmpty(t, rev)

	err = db.Delete(context.Background(), "foobar", rev)
	assert.NoError(t, err)

	var res mockMessage
	rev, err = db.Get(context.Background(), "foobar", &res)
	assert.NoError(t, err)
	assert.Empty(t, rev)
}

func Test_Database_Delete_with_empty_id(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	err := db.Delete(context.Background(), "", "")
	assert.EqualError(t, err, "id empty")
}

func Test_Database_Delete_with_empty_rev(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	err := db.Delete(context.Background(), "foobar", "")
	assert.EqualError(t, err, "rev empty")
}

func Test_Database_Delete_with_unknown_id(t *testing.T) {
	db, server := initDatabase(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), db.String()) }()

	err := db.Delete(context.Background(), "foobar", "some-rev")
	assert.NoError(t, err)
}

func Test_Database_Delete_with_unreachable_db(t *testing.T) {
	url, err := url.ParseRequestURI("http://unreachable-url")
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	err = db.Delete(context.Background(), "some-id", "some-rev")
	assert.EqualError(t, err, "fail to send request: Delete http://unreachable-url/some-database/some-id: dial tcp: lookup unreachable-url: no such host")
}

func Test_Database_Delete_with_db_failure(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts.Close()

	url, err := url.ParseRequestURI(ts.URL)
	assert.NoError(t, err)

	db := &Database{
		client:  &Client{url: url, client: http.DefaultClient},
		name:    "some-database",
		timeout: time.Second,
	}

	err = db.Delete(context.Background(), "some-id", "some-rev")
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}
