# Yet Another Couchdb client with Context

## Description

A minimal Couchdb client based on context. It aims to be battle tested and
viable for production use.

This lib haven't reach the V1 yet, some breaking changes can appear in the future.


## Documentation

Documentation and API Reference can be found on
[godoc.org](http://godoc.org/github.com/Peltoche/yaccc)


## Installation

Install couch using the "go get" command:

```
    go get github.com/Peltoche/yaccc
```


## Main Features

#### Server

- [x] Connection with retry
- [x] Ping
- [x] Mock
- [ ] Authentication
- [ ] Customizable logger


#### Database

- [x] Get from key
- [x] Set to key
- [x] Find from a raw query
- [x] Mock


## Dependencies

The only external dependencies are:

- github.com/pkg/errors (allow to print a stacktrace with the errors)
- github.com/stretchr/testify/mock (for the mocks)


## Example usage

```go
package main

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/Peltoche/yaccc"
)

type Movie struct {
	Title string `json:"title"`
	Year  int    `json:"year"`
	Score int    `json:"score"`
}

func main() {
	// Create the server
	//
	// If the connection fail, retry 3 times max with 1 second
	// between each retry.
	server, err := yaccc.NewServer("http://localhost:5984", 3, time.Second)
	if err != nil {
		panic(err)
	}

	db, err := server.ConnectOrCreateDatabase(context.TODO(), "movies")
	if err != nil {
		panic(err)
	}

	movieToSave := Movie{Title: "The Godfather", Year: 1972, Score: 90}

	_, err = db.Set(context.TODO(), "some-big-UUID", "", movieToSave)
	if err != nil {
		panic(err)
	}

	var res Movie
	_, err = db.Get(context.TODO(), "some-big-UUID", &res)
	if err != nil {
		panic(err)
	}

	fmt.Printf("result => %s\n", res.Title) // result => The Godfather

	server.DestroyDatabase(context.TODO(), "movies")
}
```

Contributing
------------

Pull requests are welcome. Please use the [gitlab repository](https://gitlab.com/Peltoche/yaccc)
to make  Pull request and to create the Issues.
