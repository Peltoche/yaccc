package yaccc

import (
	"context"
	"fmt"
	"log"
	"time"
)

func ExampleSave_and_update() {
	type Movie struct {
		Title string `json:"title"`
		Year  int    `json:"year"`
		Score int    `json:"score"`
	}

	// Create the server
	//
	// If the connection fail, retry 3 times max with 1 second
	// between each retry.
	server, err := NewClient(ClientURL, 3, time.Second)
	if err != nil {
		panic(err)
	}

	db, err := server.CreateDatabase(context.TODO(), &CreateDatabaseCmd{
		Name: "movies",
	})
	if err != nil {
		panic(err)
	}

	movieToSave := Movie{
		Title: "The Godfather",
		Year:  1972,
		Score: 90,
	}

	// Save for the first time the document.
	rev, err := db.Set(context.TODO(), "some-big-UUID", "", movieToSave)
	if err != nil {
		log.Fatalf("fail to save the document")
	}

	// Update the document and save again with the revision this time
	movieToSave.Score = 100
	_, err = db.Set(context.TODO(), "some-big-UUID", rev, movieToSave)
	if err != nil {
		log.Fatalf("fail to update the document")
	}

	server.DestroyDatabase(context.TODO(), "movies")
}

func ExampleSet_and_Find() {
	type Movie struct {
		Title string `json:"title"`
		Year  int    `json:"year"`
		Score int    `json:"score"`
	}

	server, err := NewClient(ClientURL, 3, time.Second)
	if err != nil {
		panic(err)
	}

	db, err := server.CreateDatabase(context.TODO(), &CreateDatabaseCmd{
		Name: "movies",
	})
	if err != nil {
		panic(err)
	}

	movieToSave := Movie{
		Title: "The Godfather",
		Year:  1972,
		Score: 90,
	}

	// Save for the first time the document.
	_, err = db.Set(context.TODO(), "some-big-UUID", "", &movieToSave)
	if err != nil {
		panic(err)
	}

	// Retrieve all the movies with a score greater than 80.
	results, err := db.Find(context.TODO(), `{
		"selector": {
			"score": {
				"$gt": 80
			}
		}
	}`)
	if err != nil {
		panic(err)
	}

	var res Movie
	for results.Next() {
		id, _, err := results.UnmarshalNext(&res)
		if err != nil {
			panic(err)
		}

		fmt.Printf("%s => %s", id, res.Title)
	}

	server.DestroyDatabase(context.TODO(), "movies")

	//Output: some-big-UUID => The Godfather
}
