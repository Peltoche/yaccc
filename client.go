package yaccc

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

// ClientConfig is the configuration used to instantiate a 'Client' and its "Database".
type ClientConfig struct {
	URL        string           `yaml:"url"`
	User       string           `yaml:"user"`
	Password   string           `yaml:"password"`
	Connection ConnectionConfig `yaml:"connection"`
}

// ConnectionConfig specify the retry policy.
type ConnectionConfig struct {
	MaxRetryNumber   int           `yaml:"max-retry-number"`
	CoolDownDuration time.Duration `yaml:"cooldown-duration"`
}

// Client is used to interact with a couchdb database.
type Client struct {
	url       *url.URL
	client    *http.Client
	user      string
	password  string
	cookieJar *cookiejar.Jar
}

// NewClientWithConfig instantiate a new 'Client' based on the given configs.
func NewClientWithConfig(config ClientConfig) (*Client, error) {
	client, err := NewClient(config.URL, config.Connection.MaxRetryNumber, config.Connection.CoolDownDuration)
	if err != nil {
		return nil, err
	}

	if config.User != "" || config.Password != "" {
		err = client.Authenticate(config.User, config.Password)
	}

	return client, err
}

// NewClient instantiate a new 'Client' with the given arguments.
func NewClient(serverURL string, nbRetry int, sleepDuration time.Duration) (*Client, error) {
	var (
		server    Client
		dbBaseURL *url.URL
		i         int
		err       error
	)

	dbBaseURL, err = url.ParseRequestURI(serverURL)
	if err != nil {
		return nil, err
	}

	server = Client{
		url:    dbBaseURL,
		client: &http.Client{},
	}

	for {
		i++

		err = server.Ping()
		if err == nil {
			break
		}

		if i >= nbRetry {
			return nil, errors.Wrapf(err, "failed to connect a couchdb at %q", serverURL)
		}

		log.Printf("failed to reach the couchdb server, retry after %s...\n", sleepDuration)
		time.Sleep(sleepDuration)
	}

	return &server, nil
}

// Authenticate the requests.
func (t *Client) Authenticate(user string, password string) error {
	if user == "" || password == "" {
		return errors.New("missing the user or password config")
	}

	jar, err := cookiejar.New(nil)
	if err != nil {
		return err
	}

	t.cookieJar = jar
	t.user = user
	t.password = password

	return nil
}

// SetClient configure a new http client for the server.
func (t *Client) SetClient(client *http.Client) *Client {
	t.client = client
	return t
}

// String is an implementation of 'fmt.Stringer'.
func (t *Client) String() string {
	return t.url.String()
}

// Ping check if the connection with the server is ok.
func (t *Client) Ping() error {
	var (
		err error
		res *http.Response
	)

	res, err = t.client.Get(t.url.String())
	if err != nil {
		return errors.Wrap(err, "failed to connect to the server")
	}

	var body map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&body)
	if err != nil || body["couchdb"] != "Welcome" {
		return errors.New("invalid content in response body")
	}

	return nil
}

// ConnectDatabase will start a connection to the database with the given name.
func (t *Client) ConnectDatabase(ctx context.Context, name string) (*Database, error) {
	if name == "" {
		return nil, errors.New("empty database name")
	}

	databaseURL := fmt.Sprintf("/%s", url.PathEscape(name))

	res, err := t.request(ctx, http.MethodHead, databaseURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "fail to send request")
	}
	_ = res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		database := Database{
			client: t,
			name:   name,
		}
		return &database, nil

	case http.StatusNotFound:
		return nil, errors.Errorf("database %q doesn't exist", name)

	default:
		return nil, errors.Errorf("unexpected response: %s", res.Status)
	}
}

// CreateDatabaseCmd is the required configuration to create a new database.
type CreateDatabaseCmd struct {
	Name            string                    `json:"name" yaml:"name"`
	DesignDocuments map[string]DesignDocument `json:"design-documents" yaml:"design-documents"`
}

// Language use for the DesignDocuments.
type Language string

// Languages availables for the DesignDocuments.
const (
	Javascript Language = "javascript"
	Erlang     Language = "erlang"
)

// DesignDocument regrouping views definitions.
type DesignDocument struct {
	Language Language        `json:"language" yaml:"language"`
	Views    map[string]View `json:"views" yaml:"views"`
}

// View definition.
type View struct {
	Map    string `json:"map,omitempty" yaml:"map,omitempty"`
	Reduce string `json:"reduce,omitempty" yaml:"reduce,omitempty"`
}

// CreateDatabase will create a database with the given name.
//
// If the database already exist, the method will fail with an error. If you
// want to ensure that the database exists, use the 'Ensure' method instead.
func (t *Client) CreateDatabase(ctx context.Context, cmd *CreateDatabaseCmd) (*Database, error) {
	if cmd.Name == "" {
		return nil, errors.New("empty database name")
	}

	databaseURL := fmt.Sprintf("/%s", url.PathEscape(cmd.Name))

	res, err := t.request(ctx, http.MethodPut, databaseURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "fail to send request")
	}
	_ = res.Body.Close()

	var database Database
	switch res.StatusCode {
	case http.StatusCreated:
		database = Database{
			client: t,
			name:   cmd.Name,
		}

	case http.StatusPreconditionFailed:
		return nil, fmt.Errorf("database %q already exist", cmd.Name)

	default:
		return nil, errors.Errorf("unexpected response: %s", res.Status)
	}

	for name, designDocument := range cmd.DesignDocuments {
		_, err = database.Set(ctx, "_design/"+name, "", &designDocument)
		if err != nil {
			return nil, errors.Errorf("failed to create the design document %q: %s", name, err)
		}
	}

	return &database, nil
}

// DestroyDatabase destroy the database given in parameter with all it content.
//
// This method is idempotent. This means that if the database is not found it
// will return nil.
func (t *Client) DestroyDatabase(ctx context.Context, name string) error {
	if name == "" {
		return errors.New("empty database name")
	}

	databaseURL := fmt.Sprintf("/%s", url.PathEscape(name))

	res, err := t.request(ctx, http.MethodDelete, databaseURL, nil)
	if err != nil {
		return errors.Wrap(err, "fail to send request")
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK, http.StatusNotFound:
		return nil

	default:
		return errors.Errorf("unexpected response: %s", res.Status)
	}
}

func (t *Client) request(ctx context.Context, method string,
	path string, bodyReader io.Reader) (*http.Response, error) {

	req := t.forgeRequest(ctx, method, path, bodyReader)

	res, err := t.client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, errors.Wrap(err, "fail to send request")
	}

	// save new cookies
	t.cookieJar.SetCookies(req.URL, res.Cookies())

	return res, err
}

func (t *Client) forgeRequest(ctx context.Context, method string,
	path string, bodyReader io.Reader) *http.Request {
	var (
		body io.ReadCloser
	)

	// nolint
	pathUrl, _ := url.Parse(path)

	if bodyReader == nil {
		body = nil
	} else {
		body = ioutil.NopCloser(bodyReader)
	}

	req := &http.Request{
		Method: method,
		URL:    t.url.ResolveReference(pathUrl),

		// Use HTTP 1.1
		ProtoMajor: 1,
		ProtoMinor: 1,

		// Set headers
		Header: map[string][]string{
			"Accept":       {"application/json"},
			"Content-Type": {"application/json"},
		},

		Body: body,
	}

	// basic auth
	if t.user != "" && t.password != "" {
		req.SetBasicAuth(t.user, t.password)
	}

	return req
}
