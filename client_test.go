package yaccc

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

var ClientURL = "http://localhost:5984"

func generateDBName(t *testing.T) string {
	name := strings.Replace(strings.ToLower(t.Name()), "_", "-", -1)
	return fmt.Sprintf("%s-%s", name, uuid.NewV4().String()[:5])
}

func Test_Client_String(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	assert.Equal(t, ClientURL, server.String())
}

func Test_NewCouchdbClient_with_a_booting_db(t *testing.T) {
	var (
		dbMock *httptest.Server
		retry  int
	)

	dbMock = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if retry < 2 {
			retry++
			dbMock.CloseClientConnections()
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{
			"couchdb":"Welcome",
			"version":"2.1.1",
			"features":["scheduler"],
			"vendor":{"name":"The Apache Software Foundation"}
		}`))
	}))
	defer dbMock.Close()

	server, err := NewClient(dbMock.URL, 3, time.Millisecond)

	assert.NotEmpty(t, server)
	assert.NoError(t, err)
	assert.Equal(t, 2, retry)
}

func Test_NewCouchdbClient_with_an_unreachable_server(t *testing.T) {
	var (
		dbMock *httptest.Server
		retry  int
	)

	dbMock = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		retry++
		// failed each time
		dbMock.CloseClientConnections()
	}))
	defer dbMock.Close()

	server, err := NewClient(dbMock.URL, 3, time.Millisecond)

	assert.Empty(t, server)
	assert.EqualError(t, err, fmt.Sprintf("failed to connect a couchdb at %q: failed to connect to the server: Get %s: EOF", dbMock.URL, dbMock.URL))
	assert.Equal(t, 3, retry)
}

func Test_NewClient_with_invalid_url(t *testing.T) {
	server, err := NewClient("invalid-url#foobar", 2, time.Millisecond)
	assert.Nil(t, server)
	assert.EqualError(t, err, "parse invalid-url#foobar: invalid URI for request")
}

func Test_NewClient_with_an_invalid_response_format(t *testing.T) {
	dbMock := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`"not a json`))
	}))
	defer dbMock.Close()

	server, err := NewClient(dbMock.URL, 3, time.Millisecond)

	assert.Nil(t, server)
	assert.EqualError(t, err, fmt.Sprintf("failed to connect a couchdb at %q: invalid content in response body", dbMock.URL))
}

func Test_NewClientWithConfig(t *testing.T) {
	config := ClientConfig{
		URL: ClientURL,
		Connection: ConnectionConfig{
			MaxRetryNumber:   2,
			CoolDownDuration: time.Millisecond,
		},
	}

	server, err := NewClientWithConfig(config)

	assert.NoError(t, err)
	assert.NotEmpty(t, server)
}

func Test_NewClientWithConfig_with_error(t *testing.T) {
	config := ClientConfig{
		URL: "http://unreachable-url/",
		Connection: ConnectionConfig{
			MaxRetryNumber:   2,
			CoolDownDuration: time.Millisecond,
		},
	}

	server, err := NewClientWithConfig(config)

	assert.EqualError(t, err, `failed to connect a couchdb at "http://unreachable-url/": failed to connect to the server: Get http://unreachable-url/: dial tcp: lookup unreachable-url: no such host`)
	assert.Nil(t, server)
}

func Test_Client_ConnectDatabase(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	dbName := generateDBName(t)
	server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: dbName})

	db, err := server.ConnectDatabase(context.Background(), dbName)
	assert.NotEmpty(t, db)
	assert.NoError(t, err)

}

func Test_Client_ConnectDatabase_with_empty_database_name(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	db, err := server.ConnectDatabase(context.Background(), "")
	assert.Nil(t, db)
	assert.EqualError(t, err, "empty database name")
}

func Test_Client_ConnectDatabase_with_unexisting_database(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	db, err := server.ConnectDatabase(context.Background(), "some-database")
	assert.Nil(t, db)
	assert.EqualError(t, err, `database "some-database" doesn't exist`)

}

func Test_Client_ConnectDatabase_with_server_error(t *testing.T) {
	mockedDB := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer mockedDB.Client()

	url, err := url.ParseRequestURI(mockedDB.URL)
	assert.NoError(t, err)

	server := &Client{
		url:    url,
		client: http.DefaultClient,
	}
	assert.NoError(t, err)

	db, err := server.ConnectDatabase(context.Background(), "some-database")
	assert.Nil(t, db)
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}

func Test_Client_CreateDatabase(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	dbName := generateDBName(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), dbName) }()

	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: dbName})

	assert.NotNil(t, db)
	assert.NoError(t, err)
}

func Test_Client_CreateDatabase_with_design_document(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	dbName := generateDBName(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), dbName) }()

	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{
		Name: dbName,
		DesignDocuments: map[string]DesignDocument{
			"default": {
				Language: Javascript,
				Views: map[string]View{
					"all": {
						Map: "function(doc) { if(doc.type == 'article') { emit(null, doc); }  }",
					},
				},
			},
		},
	})

	assert.NotNil(t, db)
	assert.NoError(t, err)
}

func Test_Client_CreateDatabase_with_empty_database_name(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: ""})
	assert.Nil(t, db)
	assert.EqualError(t, err, "empty database name")
}

func Test_Client_CreateDatabase_with_db_failure(t *testing.T) {
	mockedDB := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer mockedDB.Close()

	url, err := url.ParseRequestURI(mockedDB.URL)
	assert.NoError(t, err)

	server := &Client{
		url:    url,
		client: http.DefaultClient,
	}

	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: "some-database"})

	assert.Nil(t, db)
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}

func Test_Client_CreateDatabase_already_exist(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	dbName := generateDBName(t)
	defer func() { _ = server.DestroyDatabase(context.Background(), dbName) }()

	// First creation
	db, err := server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: dbName})
	assert.NotNil(t, db)
	assert.NoError(t, err)

	// Second, should fail
	db, err = server.CreateDatabase(context.Background(), &CreateDatabaseCmd{Name: dbName})

	assert.Nil(t, db)
	assert.EqualError(t, err, fmt.Sprintf("database %q already exist", dbName))
}

func Test_Client_DestroyDatabase_with_empty_database_name(t *testing.T) {
	server, err := NewClient(ClientURL, 0, time.Second)
	assert.NoError(t, err)

	err = server.DestroyDatabase(context.Background(), "")
	assert.EqualError(t, err, "empty database name")
}

func Test_Client_DestroyDatabase_with_db_failure(t *testing.T) {
	mockedDB := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer mockedDB.Close()

	url, err := url.ParseRequestURI(mockedDB.URL)
	assert.NoError(t, err)

	server := &Client{
		url:    url,
		client: http.DefaultClient,
	}

	err = server.DestroyDatabase(context.Background(), "some-database")
	assert.EqualError(t, err, "unexpected response: 500 Internal Server Error")
}
