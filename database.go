package yaccc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Database is used to interact with a couchdb database.
type Database struct {
	client  *Client
	timeout time.Duration
	name    string
}

// SetTimeout for request deadline.
//
// Once the timeout is reached, the request is aborted via the context. An
// empty duration is equivalent to no timeout set.
func (t *Database) SetTimeout(timeout time.Duration) {
	t.timeout = timeout
}

// String is an implementation of 'fmt.Stringer'.
func (t *Database) String() string {
	return t.name
}

// Name return the database name.
func (t *Database) Name() string {
	return t.name
}

// DatabaseInfo returned by the Info method.
type DatabaseInfo struct {
	Name              string      `json:"db_name"`
	Cluster           ClusterInfo `json:"cluster"`
	CompactRunning    bool        `json:"compact_running"`
	DiskFormatVersion int         `json:"disk_format_version"`
	DocCount          int         `json:"doc_count"`
	DocDelCount       int         `json:"doc_del_count"`
	InstanceStartTime string      `json:"instance_start_time"`
	PurgeSeq          int         `json:"purge_seq"`
	Sizes             SizesInfo   `json:"sizes"`
	UpdateSeq         string      `json:"update_seq"`
}

// ClusterInfo part of the Info response.
type ClusterInfo struct {
	Replicas     int `json:"n"`
	Shards       int `json:"q"`
	ReadQuorums  int `json:"r"`
	WriteQuorums int `json:"w"`
}

// SizesInfo part of the Info response.
type SizesInfo struct {
	Active   int `json:"active"`
	External int `json:"external"`
	File     int `json:"file"`
}

// Info return a bunch of metadata about the database.
func (t *Database) Info(ctx context.Context) (*DatabaseInfo, error) {
	var info DatabaseInfo

	documentURL := fmt.Sprintf("/%s", url.PathEscape(t.name))

	res, err := t.client.request(ctx, http.MethodGet, documentURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "fail to send request")
	}
	defer func() { _ = res.Body.Close() }()

	switch res.StatusCode {
	case http.StatusOK:
		err = json.NewDecoder(res.Body).Decode(&info)
		if err != nil {
			return nil, errors.Wrap(err, "fail to unmarshal body")
		}

	case http.StatusNotFound:
		return nil, errors.New("database not found")

	default:
		return nil, errors.Errorf("unexpected response: %s", res.Status)
	}

	return &info, nil
}

// Set the given value with the given id.
//
// Couchdb works with the MVCC model
// (http://docs.couchdb.org/en/2.1.1/intro/api.html#revisions) and so for
// updating and already existing document, it need its revision.
//
// If it the first time the key is used, keep the rev field empty. If a document
// have been already saved with this key, give the revision returned during the
// previous call.
//
// In case of success it return the revision of the newly saved document.
//
// The value will be serialized using 'json.Marshal' so the 'json' tags set in
// your struct will be take in account.
func (t *Database) Set(
	ctx context.Context,
	id string,
	rev string,
	value interface{},
) (string, error) {

	if id == "" {
		return "", errors.New("id empty")
	}

	if reflect.TypeOf(value).Kind() != reflect.Ptr || value == nil || reflect.TypeOf(value).Elem().Kind() != reflect.Struct {
		return "", errors.New("value must be a &struct")
	}

	documentURL := fmt.Sprintf("/%s/%s", url.PathEscape(t.name), url.PathEscape(id))

	rawBody, err := json.Marshal(value)
	if err != nil {
		return "", errors.Wrap(err, "fail to encode document")
	}

	req := t.client.forgeRequest(ctx, http.MethodPut, documentURL, bytes.NewReader(rawBody))

	if rev != "" {
		req.Header.Set("If-Match", rev)
	}

	res, err := t.client.client.Do(req.WithContext(ctx))
	if err != nil {
		return "", errors.Wrap(err, "fail to send request")
	}
	_ = res.Body.Close()

	switch res.StatusCode {
	case http.StatusCreated:
		return res.Header.Get("Etag"), nil

	default:
		return "", errors.Errorf("unexpected response: %s", res.Status)
	}
}

// Get the value at the specified id and fill the given valuePtr with.
//
// In case of success it will also return the current document revision.
//
// The value will be deserialized using 'json.Unmarshal' so the 'json' tags set
// on your struct will be take in account.
func (t *Database) Get(
	ctx context.Context,
	id string,
	valuePtr interface{},
) (string, error) {
	if id == "" {
		return "", errors.New("id empty")
	}

	documentURL := fmt.Sprintf("/%s/%s", url.PathEscape(t.name), url.PathEscape(id))

	res, err := t.client.request(ctx, http.MethodGet, documentURL, nil)
	if err != nil {
		return "", err
	}
	defer func() { _ = res.Body.Close() }()

	switch res.StatusCode {
	case http.StatusOK:
		err = json.NewDecoder(res.Body).Decode(&valuePtr)
		if err != nil {
			return "", errors.Wrap(err, "fail to unmarshal body")
		}

		return res.Header.Get("Etag"), nil

	case http.StatusNotFound:
		return "", nil

	default:
		return "", errors.Errorf("unexpected response: %s", res.Status)
	}
}

// Delete the given the content of the given id.
func (t *Database) Delete(
	ctx context.Context,
	id string,
	rev string,
) error {
	if id == "" {
		return errors.New("id empty")
	}

	if rev == "" {
		return errors.New("rev empty")
	}

	documentURL := fmt.Sprintf("/%s/%s", url.PathEscape(t.name), url.PathEscape(id))

	req := t.client.forgeRequest(ctx, http.MethodDelete, documentURL, nil)
	if rev != "" {
		req.Header.Set("If-Match", rev)
	}

	res, err := t.client.client.Do(req.WithContext(ctx))
	if err != nil {
		return errors.Wrap(err, "fail to send request")
	}
	_ = res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusNotFound:
		return nil

	default:
		return errors.Errorf("unexpected response: %s", res.Status)
	}
}

// Find execute a map-reduce query and return the FindResult.
//
// The query documentation can be found here :
// http://docs.couchdb.org/en/2.1.1/api/database/find.html
func (t *Database) Find(
	ctx context.Context,
	format string,
	v ...interface{},
) (*FindResult, error) {

	documentURL := fmt.Sprintf("/%s/_find", url.PathEscape(t.name))

	res, err := t.client.request(ctx, http.MethodPost, documentURL, strings.NewReader(fmt.Sprintf(format, v...)))
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		findResult, err := NewFindResult(res.Body)
		if err != nil {
			return nil, err
		}

		_ = res.Body.Close()
		return findResult, nil

	default:
		return nil, errors.Errorf("unexpected response: %s", res.Status)
	}
}

// ViewQuery contains the parameters for query a view.
type ViewQuery struct {
	DesignDocument string
	ViewName       string
	Limit          uint
	Skip           uint
	Order          SortOrder
	Keys           []interface{}
	Range          *Range
}

// SortOrder for ViewQuery
type SortOrder int

// Range for ViewQuery
type Range struct {
	Start interface{}
	End   interface{}
}

// Available SortOrder for ViewQuery.
const (
	Ascending SortOrder = iota
	Descending
)

// ViewRow returned by a QueryView.
type ViewRow struct {
	ID    string
	Key   interface{}
	Value json.RawMessage
}

type viewResult struct {
	TotalRows int       `json:"total_rows"`
	Offset    int       `json:"offset"`
	Rows      []ViewRow `json:"rows"`
}

// QueryView will query a new linked to the database.
func (t *Database) QueryView(ctx context.Context, query *ViewQuery) ([]ViewRow, error) {
	documentURL := fmt.Sprintf("/%s/_design/%s/_view/%s",
		url.PathEscape(t.Name()),
		url.PathEscape(query.DesignDocument),
		url.PathEscape(query.ViewName),
	)

	urlValues := url.Values{}

	urlValues.Add("stable", "true")
	urlValues.Add("update", "true")

	if query.Range != nil {
		if query.Range.Start != nil {
			rawStart, err := json.Marshal(query.Range.Start)
			if err != nil {
				return nil, errors.Errorf("failed to marshal the range start key: %s", err)
			}

			urlValues.Add("startkey", string(rawStart))
		}

		if query.Range.End != nil {
			rawEnd, err := json.Marshal(query.Range.End)
			if err != nil {
				return nil, errors.Errorf("failed to marshal the range end key: %s", err)
			}

			urlValues.Add("endkey", string(rawEnd))
		}
	}

	if query.Order == Descending {
		urlValues.Add("descending", strconv.FormatBool(true))
	}

	if query.Limit != 0 {
		urlValues.Add("limit", strconv.Itoa(int(query.Limit)))
	}

	if query.Skip != 0 {
		urlValues.Add("skip", strconv.Itoa(int(query.Limit)))
	}

	switch len(query.Keys) {
	case 0:
		break
	case 1:
		rawKey, err := json.Marshal(query.Keys[0])
		if err != nil {
			return nil, errors.Errorf("failed to unmarhsal keys: %s", err)
		}

		urlValues.Add("key", string(rawKey))
	default:
		rawKeys, err := json.Marshal(query.Keys)
		if err != nil {
			return nil, errors.Errorf("failed to unmarhsal keys: %s", err)
		}

		urlValues.Add("keys", string(rawKeys))
	}

	req := t.client.forgeRequest(ctx, http.MethodGet, documentURL, nil)

	req.URL.RawQuery = urlValues.Encode()

	res, err := t.client.client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, errors.Wrap(err, "fail to send request")
	}
	defer func() { _ = res.Body.Close() }()

	switch res.StatusCode {
	case http.StatusOK:
		var result viewResult

		err = json.NewDecoder(res.Body).Decode(&result)
		if err != nil {
			return nil, errors.Wrap(err, "fail to unmarshal body")
		}
		return result.Rows, nil

	case http.StatusNotFound:
		return []ViewRow{}, nil

	default:
		body, _ := ioutil.ReadAll(res.Body)
		return nil, errors.Errorf("unexpected response: %q: %s", res.Status, body)
	}
}
