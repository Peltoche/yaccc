package yaccc

import (
	"context"
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
)

// DatabaseMock is a mock implementation of 'Database' with the
// 'testify/mock' lib.
type DatabaseMock struct {
	mock.Mock
}

// Exist is a mock implementation of 'Database.Exist' method.
func (t *DatabaseMock) Exist(_ context.Context) (bool, error) {
	args := t.Called()

	return args.Bool(0), args.Error(1)
}

// Info is a mock implementation of 'Database.Info' method.
func (t *DatabaseMock) Info(_ context.Context) (*DatabaseInfo, error) {
	args := t.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*DatabaseInfo), args.Error(1)
}

// Create is a mock implementation of 'Database.Create' method.
func (t *DatabaseMock) Create(_ context.Context) error {
	return t.Called().Error(0)
}

// Ensure is a mock implementation of 'Database.Ensure' method.
func (t *DatabaseMock) Ensure(_ context.Context) error {
	return t.Called().Error(0)
}

// Destroy is a mock implementation of 'Database.Destroy' method.
func (t *DatabaseMock) Destroy(_ context.Context) error {
	return t.Called().Error(0)
}

// Set is a mock implementation of 'Database.Set' method.
func (t *DatabaseMock) Set(
	_ context.Context, id string, rev string, value interface{},
) (string, error) {
	args := t.Called(id, rev, value)

	return args.String(0), args.Error(1)
}

// Get is a mock implementation of 'Database.Get' method.
func (t *DatabaseMock) Get(
	_ context.Context, id string, valuePtr interface{},
) (string, error) {
	args := t.Called(id)

	if args.Get(2) != nil || args.String(0) == "" {
		return "", args.Error(2)
	}

	body := args.String(0)
	err := json.Unmarshal([]byte(body), valuePtr)
	if err != nil {
		return "", errors.Wrap(err, "fail to unmarshal body")
	}

	return args.String(1), args.Error(2)
}

// Find is a mock implementation of 'Database.Find' method.
func (t *DatabaseMock) Find(
	_ context.Context,
	format string,
	v ...interface{},
) (*FindResult, error) {

	args := t.Called(v...)

	if args.Get(1) != nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*FindResult), args.Error(1)
}
